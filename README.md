Install Plugin:

VERSION 1:

cordova plugin add D:\Projects\Android\cordova-plugin-notification-service

cordova plugin add https://android-dreamliner@bitbucket.org/android-dreamliner/cordova-plugin-notification-service.git
cordova plugin remove cordova-plugin-notification-service

Fire event INSTALL_REFERRER:

CMD:
D:\AndroidSDK\platform-tools\adb shell

For Background Test APK:
am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.background/com.xxx.notification.service.Receiver --es "referrer" "textinreferrer"

For others apps or cordova, change 'com.example.hello' to your app package found in manifest or id in config file:
am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.hello/com.xxx.notification.service.Receiver --es "referrer" "textinreferrer"