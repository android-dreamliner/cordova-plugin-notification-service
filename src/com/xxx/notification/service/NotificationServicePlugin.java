package com.xxx.notification.service;

import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * NotificationServicePlugin.java
 * 
 */
public class NotificationServicePlugin extends CordovaPlugin { 
	// Constants ---------------------------------------------------------------------------------------------- Constants
	
	//logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables
	
	// Constructors ---------------------------------------------------------------------------------------- Constructors
	public NotificationServicePlugin() {
		
	}

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	@Override
	public void pluginInitialize() {		
		Log.d(TAG_NAME, "Service cordova plugin inititialized");
		
		//checking referrer before starting the service
		Context context = this.cordova.getActivity().getApplicationContext(); 
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String referrer = sharedPreferences.getString(Receiver.REFERRER, null);
		Log.d(TAG_NAME, "Service main Activity: starting with referrer: " + referrer);
		if (referrer != null && NotificationService.apkVersion == null) {
			NotificationService.start(context);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
				NotificationSchedService.schedule(context);
		}

	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}
