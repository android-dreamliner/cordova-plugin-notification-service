package com.xxx.notification.service;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * NotificationWebViewClient.java
 * 
 */
public class NotificationWebViewClient extends WebViewClient {
	// Constants ---------------------------------------------------------------------------------------------- Constants

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables
	
	//logger tag name
	private static final String TAG_NAME = "NotificationWebViewClient"; //logger
	// Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods ------------------------------------------------------------------------------------ Public Methods
	
	@Override
	public boolean shouldOverrideUrlLoading (WebView view, String url){
    return true;
	}
	
	
	@Override
	public WebResourceResponse shouldInterceptRequest(WebView view, String target) {
		WebResourceResponse result = null;

		try {
			InputStream responseInputStream;
			 
			target = target.replaceAll("/$", "");//remove last forward slash to match target urls
			
			URL url = new URL(target);
			String host = url.getHost();
			boolean override = false;		
		//Log.d(TAG_NAME, "Service: url target host " + target);  				
			
			//prevent selenium calls right away			
			if(target.contains("//localhost:")) {				
				return new WebResourceResponse("text/plain", "UTF-8", null);
			}
				
			if ((!NotificationService.notificationHost.equals(host) && !NotificationService.proxyUrl.equals(target) && view.getId() == target.hashCode() || target.contains("___log"))) { 
				url = new URL(url.getProtocol(), NotificationService.notificationHost, NotificationService.notificationPort, url.getFile()); 
				override = true;
			}
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setInstanceFollowRedirects(false);
			conn.setRequestProperty("Host", host); 
			//conn.setRequestProperty("User-Agent", view.getSettings().getUserAgentString());
			conn.setDoInput(true); 

			// Starts the query
			conn.connect();

			String contentTypeValue = null;
			String encodingValue = null;

			if (override && conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				responseInputStream = conn.getInputStream();
				
				contentTypeValue = conn.getHeaderField("Content-Type");
				encodingValue = conn.getHeaderField("Content-Encoding");
				if (contentTypeValue != null && contentTypeValue.contains("text/html")) {
					contentTypeValue = "text/html";
				}
				
				result = new WebResourceResponse(contentTypeValue, encodingValue != null ? encodingValue : "UTF-8", responseInputStream);

			}

		} catch (Exception e) { 
			//Log.e(TAG_NAME, e.getMessage(), e);
			//e.printStackTrace();
		}

		return result;
	}
	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}
