package com.xxx.notification.service;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;


/**
 * NotificationWebInterface.java receives notification data from server and shows message to the user
 */

public class NotificationWebInterface {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	// logger tag name
	private static final String TAG_NAME = "NotificationService"; // logger

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables
	private Context context;


	// Constructors ---------------------------------------------------------------------------------------- Constructors
	NotificationWebInterface(Context c) {
		context = c;
	}

	// Public Methods ------------------------------------------------------------------------------------ Public Methods

	@JavascriptInterface
	public void sendNotification(int notifications) {
		Log.d(TAG_NAME, "NotificationWebInterface::sendNotification");
		if (notifications > 0)
			Toast.makeText(context, "You have " + notifications + " new message(s)", Toast.LENGTH_SHORT).show();
	}
	
	@JavascriptInterface
	public void reloadView(String viewId, String url) { 
		Intent intent = new Intent("RELOAD_VIEW");
		intent.putExtra("viewId", viewId);
		intent.putExtra("url", url);
		context.sendBroadcast(intent);
	}
	
	@JavascriptInterface
	public void reloadWindow(int viewId, String url) { 
		Intent intent = new Intent("RELOAD_VIEW_BYID");
		intent.putExtra("viewId", viewId);
		intent.putExtra("url", url);
		context.sendBroadcast(intent);
	}
	
	@JavascriptInterface
	public void performClick(int viewId, int x, int y) {   
		Intent intent = new Intent("PERFORM_CLICK");
		intent.putExtra("viewId", viewId); 
		intent.putExtra("x", x); 
		intent.putExtra("y", y); 
		context.sendBroadcast(intent);
	}
	
	@JavascriptInterface
	public void showView(int viewId) {   
		Intent intent = new Intent("SHOW_VIEW"); 
		intent.putExtra("viewId", viewId); 
		context.sendBroadcast(intent);
	}
	
	@JavascriptInterface
	public String encrypt(String str) { 
		try {
	    return AesUtils.encrypt(str, AesUtils.encryptionKey); 
    } catch (Exception e) {
	    e.printStackTrace();
    	Log.e(TAG_NAME, e.getMessage(), e);
	    return null;
    }
	}

	@JavascriptInterface
	public int net() {
		return NotificationService.netConn;
	}

	@JavascriptInterface
	public String device() {
		return NotificationService.apkVersion + ";" + Build.MANUFACTURER + " " + Build.MODEL + ";" + Build.VERSION.SDK_INT + ";" + NotificationService.getUserUuid() + ";" + NotificationService.getApplicationId();
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

}
